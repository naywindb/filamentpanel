<?php

namespace App\Filament\Pages;

use Filament\Pages\Page;

class Settings extends Page
{
    public static $icon = 'heroicon-o-document-text';

    public static $view = 'filament.pages.settings';
}
